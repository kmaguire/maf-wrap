#script to submit and monitor multicore G-FACT 
#
#Kevin Maguire
#10/04/2015
#Version 1.1


#Usage:
# Can be used to submit a job, see submitMulticore.py as an example
# Can be used interactively in python environment
# >> python -i multicore.py
# >> jobs()
# >> help()

#Add:
# * Fix the jobs function so it works better with the removeJob function
# * Allow the user to point to different config which will allow jobs to be resubmitted
#   or rerun with very small changes

#Note:
# * pickle doesn't work for these classes, so dont bother adding it
 
#------------------------------------------------------------------

import os, sys
import subprocess
from random import randint

#------------------------------------------------------------------

JDL_NAME = 'mcore.jdl'
SH_NAME = 'mcore.sh'
main_gfactdir = os.environ['GFACTDIR']
folder = 'jobs/'
UNAME = 'NONE'
for i, string in enumerate(main_gfactdir.split(os.sep)):
    if string == 'user':
        UNAME = main_gfactdir.split(os.sep)[i+2]
#JOB_LOCATION = main_gfactdir+'/G-Fact/tools/Multicore/'+folder
JOB_LOCATION = '/afs/cern.ch/work/'+UNAME[0]+'/'+UNAME+'/ycp2012/multicoreJobs/'
MANVOLOC = 'srm://bohr3226.tier2.hep.manchester.ac.uk/dpm/tier2.hep.manchester.ac.uk/home/vo.northgrid.ac.uk/'+UNAME+'/'+folder
#massPlotLocation =  'srm://bohr3226.tier2.hep.manchester.ac.uk/dpm/tier2.hep.manchester.ac.uk/home/vo.northgrid.ac.uk/masmith/exclMass/'
#massPlotLocation =  'srm://bohr3226.tier2.hep.manchester.ac.uk/dpm/tier2.hep.manchester.ac.uk/home/vo.northgrid.ac.uk/masmith/std_241115/'
#massPlotLocation =  'srm://bohr3226.tier2.hep.manchester.ac.uk/dpm/tier2.hep.manchester.ac.uk/home/vo.northgrid.ac.uk/masmith/std_241115/'
#timePlotsLocation = '/afs/cern.ch/work/m/masmith/yCP/stdFits_110815/'
MODES = ['kk','pipi']
MAGS = ['MagUp','MagDown']
RUNS = ['run1','run2','run3']

#------------------------------------------------------------------

#--------------------------------------------------------------------------
# Decorators
#--------------------------------------------------------------------------

def checkForDir(func):
    '''
    decorator to check if job files exists and make it.
    bit of a hack, for decorator practice
    '''
    def wrapper(job):
        if not os.path.isdir(JOB_LOCATION+os.sep+str(job.id)):
            os.system('mkdir {0}'.format(JOB_LOCATION+os.sep+str(job.id)))
            return
        else:
            func(job)
        return 
    return wrapper
        

#--------------------------------------------------------------------------
# Job class
#--------------------------------------------------------------------------

class Job(object):
    joblist = []
    """
    class with all the job info

    The exe is a paths to the exe file or the exe name
    after it has been copied to the job directory
    vostore is the path to where the job output will be stored on the manchester VO storage

    """
    def __init__(self, jid = None, url = '', name = '', exe = '', args = '',timePlotsFile = '', jdl = '', sh = '', sandbox = '', output = '',
                 status = '', outputStatus = '', loaded=False):
        if jid == None:
            self.id = len(Job.joblist)
        else:
            self.id = jid
        self.url = url
        self.name = name
        self.exe = exe
        self.args = args
#        self.timePlots = massPlotLocation+timePlotsFile
        self.jdl = jdl
        self.sh = sh
        self.sandbox = sandbox
        self.status = status
        self.outputStatus = outputStatus
        self.loaded = loaded
#        self.massPlots = massPlotLocation+timePlotsFile
        Job.joblist.append(self)
        if self.loaded == False:
            #make the job dir with the name sh exe and jdl files
            if os.system('mkdir {0}'.format(JOB_LOCATION+str(self.id))) == 256: #already exist
                print('WARN: Job::__init__: Writing over old job, ID {0},  which was not submitted'.format(self.id))
                os.system('rm -r {0}'.format(JOB_LOCATION+str(self.id)))
                os.system('mkdir {0}'.format(JOB_LOCATION+str(self.id)))
            self.setName(name)
            self.getEXE()
        #        self.gettimePlots()
            self.setVOStore()
            self.setOutput(output)
            self.otherArgs()

    #------------------------------------------------------------------------
    # Main Job functions
    #------------------------------------------------------------------------            

    def write(self):
        '''write the submission and bash files'''
        self.writeSH()
        self.writeJDL()
        
    def getName(self):
        '''Gets the name of the job from the txt file'''        
        if not os.path.isdir(JOB_LOCATION+str(self.id)+os.sep):
            os.system('mkdir {0}'.format(JOB_LOCATION+str(self.id)+os.sep))
        path = JOB_LOCATION+str(self.id)+os.sep 
        namefile = open(path+'name', 'rb')
        name = namefile.readlines()[0].rstrip('\n')
        self.name = name
        return

    def getVOStore(self):
        '''Gets the vostore dir from the txt file'''

        if not os.path.isdir(JOB_LOCATION+str(self.id)+os.sep):
            os.system('mkdir {0}'.format(JOB_LOCATION+str(self.id)+os.sep))
        path = JOB_LOCATION+str(self.id)+os.sep
        volocfile = open(path+'vostore', 'rb')
        voloc = volocfile.readlines()[0].rstrip('\n')
        self.vostorename = voloc
        return

    def readOutputName(self):
        '''Gets the root output name of the job from the txt file'''
        
        if not os.path.isdir(JOB_LOCATION+str(self.id)+os.sep):
            os.system('mkdir {0}'.format(JOB_LOCATION+str(self.id)+os.sep))
        path = JOB_LOCATION+str(self.id)+os.sep
        outputnamef = open(path+'rootoutput', 'rb')
        output = outputnamef.readlines()[0].rstrip('\n')
        self.output = output
        return

    @checkForDir        
    def getJDL(self):
        '''set job.jdl to the jdl file path'''
        path = JOB_LOCATION+str(self.id)+os.sep
        if os.path.isfile(path+JDL_NAME) == 1:
            jdl = JDL_NAME
        elif self.name != 'removed':
            print("WARN: Job::getJDL: Can't find JDL for JOB {0}".format(self.id))
        return

    @checkForDir
    def getSH(self):
        '''set job.sh to the SH file path'''
        path = JOB_LOCATION+str(self.id)+os.sep
        if os.path.isfile(path+SH_NAME) == 1:
            self.sh = SH_NAME
        elif self.name != 'removed':
            print("WARN: Job::getSH: Can't find SH file for JOB {0}".format(self.id))
        return

    def getEXE(self):
        #copies the exe from it's location to the current location
        #adds the exe to the sandbox
        #sets the exe atribute to the file name

        exe = self.exe
        os.system('cp {0} {1}'.format(exe, JOB_LOCATION+str(self.id)+os.sep))
        exename = os.path.basename(exe)
        self.sandbox += '"{0}",'.format(JOB_LOCATION+str(self.id)+os.sep+exename)
        self.exe = exename
        return

    def gettimePlots(self):
        #copies the exe from it's location to the current location
        #adds the exe to the sandbox
        #sets the exe atribute to the file name

        timePlots = self.timePlots
#        os.system('cp {0} {1}'.format(timePlots, JOB_LOCATION+str(self.id)+os.sep+'timePlots.root'))
#        exename = os.path.basename(exe)
        self.sandbox += '"{0}",'.format(timePlotsLocation + self.timePlots)
#        self.sandbox += '"{0}",'.format(JOB_LOCATION+str(self.id)+os.sep+'timePlots.root')
        timePlotsname = os.path.basename(timePlots)
        self.timePlots = timePlotsname
        
        return
        
    def setupData(self, dataFiles):
        '''set the data files as args'''

        if len(dataFiles)==4:
          self.args += ' --signalFile {0} {1}'.format(dataFiles[0], dataFiles[1])
        else:
          self.args += ' --signalFile {0}'.format(dataFiles[0])
        if len(dataFiles) == 1:
            return
        if len(dataFiles)==4:
          self.args += ' --wideMassFile {0} {1}'.format(dataFiles[2], dataFiles[3])
        else:
          self.args += ' --wideMassFile {0}'.format(dataFiles[1])

    def otherArgs(self):
        '''some misc args'''
        #make sure it runs on 8 threads
        self.args += " --nThreads 8 "

    def setName(self, name = ''):
        '''
        Set the name in the name file.
        Can ovveride self.name is name is given
        '''
        if name == '':
            jobname = self.name
        else:
            jobname = name

        path = JOB_LOCATION+str(self.id)+os.sep
        os.system('printf "{0}" > {1}'.format(jobname.rstrip('\n'), path+'name'))
        return

    def setVOStore(self):
        '''Set the output base name on the VO storage'''

        voname = str( randint(100000,999999) )
        os.system('printf "{0}" > {1}'.format(voname.rstrip('\n'), JOB_LOCATION+str(self.id)+os.sep+'vostore'))
        self.vostorename = voname

        return

    def setOutput(self, output):
        '''Set the root output name and loc'''

        os.system('printf "{0}" > {1}'.format(output.rstrip('\n'), JOB_LOCATION+str(self.id)+os.sep+'rootoutput'))
        self.output = output

        return

    def writeJDL(self):
        '''Make the JDL submission txt file'''

        path = JOB_LOCATION+str(self.id)+os.sep
        name = JDL_NAME
        jdl = open(path+name, 'wb')
        jdl.write('[\n') 
        jdl.write('Arguments = "";\n') 
        jdl.write('Executable = "{0}";\n'.format(self.sh))
        #make sure you get rid of the last ',' in sandbox 
        jdl.write('InputSandbox = {{{0}}};\n'.format(self.sandbox[:-1]))
        jdl.write('StdOutput = "stdout.log";\n')
        jdl.write('StdError = "stderr.log";\n')
        jdl.write('OutputSandbox = {};\n')
        jdl.write('OutputSandboxBaseDestURI = "gsiftp://localhost";\n')
        jdl.write('CPUNumber=8;\n')
        jdl.write('HostNumber=1\n')
        jdl.write(']\n')
        self.jdl = JDL_NAME
        return

    def writeSH(self):
        '''Make the exe bash script'''
        
        path = JOB_LOCATION+str(self.id)+os.sep
        name = SH_NAME
        sh = open(path+name, 'wb')
        sh.write('#!/bin/sh\n')
        sh.write('mkdir G-Fact\n')
        sh.write('mkdir G-Fact/configurations\n')
        sh.write('mkdir outputs\n')
        sh.write('mv *.txt G-Fact/configurations/\n')
        sh.write('source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c x86_64-slc6-gcc49-opt\n')
        sh.write('SetupProject ROOT 6.06.02\n')
        sh.write('export GFACTDIR=${PWD}\n')
        sh.write('echo $GFACTDIR\n')
#        sh.write('gfal-copy  --verbose {0}  file://$GFACTDIR/timePlots.root \n'.format(self.timePlots))
#        sh.write('gfal-copy  --verbose {0}  file://$GFACTDIR/massPlots.root \n'.format(self.massPlots))
        sh.write('chmod u+x {0}\n'.format(self.exe))
        sh.write('ls\n')
        sh.write('./{0} {1}\n'.format(self.exe, self.args))
        sh.write('ls\n')
        vostoreroot = self.vostorename+'.root'
        vostorelog = self.vostorename+'.log'
        vostoreerr = self.vostorename+'.err'
        sh.write('gfal-copy --verbose file://$GFACTDIR/output.root {0}\n'.format(MANVOLOC+vostoreroot))
        sh.write('gfal-copy --verbose file://$GFACTDIR/stdout.log {0}\n'.format(MANVOLOC+vostorelog))
        sh.write('gfal-copy --verbose file://$GFACTDIR/stderr.err {0}\n'.format(MANVOLOC+vostoreerr))
        sh.write('rm -r G-Fact \n')
        sh.write('rm output.root \n')
        sh.write('rm tpSPlots.root \n')
        sh.write('rm -r outputs \n')
        sh.write('rm stdout.log \n')
        sh.write('rm stderr.log \n')
        sh.write('rm G-Fact.exe \n')
        sh.write('rm timePlots.root \n')
#        sh.write('rm {0} \n'.format(self.timePlots))
        self.sh = name
        self.sandbox += '"{0}",'.format(path+self.sh)
        return

    #--------------------------------------------------------------------------
    # Submit batch function
    #--------------------------------------------------------------------------

    def getConfigArgs(self, configFiles = []):
        '''
        Makes a structured directory to hold all the options files for the job
        and makes an str of the config args
        if there are no configFiles it looks in G-Fact-Config
        '''

        #paths
        mainConfigFileName = "G-Fact-Config.txt"
        gfactDirectory = main_gfactdir
        configDir = gfactDirectory + os.sep + 'G-Fact' + os.sep + 'configurations' 
        
        #make list of config file locations if not done already
        newconfigFiles = []
        if configFiles == []:
            #parse the main config file to get the configuration files.
            mainConfigFile = open( configDir + os.sep + mainConfigFileName, "r" )
            mainConfigLines = mainConfigFile.readlines()
            for line in mainConfigLines:
                if line.find( 'configurationFiles' ) != -1 and line[0] != "*" :
                    for file in line.split()[1:]:
                        newconfigFiles.append( file.replace('\n','').replace('\t','') )
            mainConfigFile.close()
            configFiles = newconfigFiles
            
        #make a location for the config files
        oldpwd = os.getcwd()
        os.chdir(JOB_LOCATION+str(self.id))
        pwd = os.getcwd()
        os.mkdir('G-Fact')
        os.mkdir('G-Fact'+os.sep+'configurations')

        #copy the config files
        os.system('cp '+configDir+os.sep+mainConfigFileName+' ./')
        configFilesString = ''
        sandbox = ''
        #print configFiles
        for file in configFiles:
            os.system( 'cp ' + configDir + os.sep + file + ' ' + pwd + os.sep
                       + 'G-Fact' + os.sep + 'configurations' + os.sep )
            configFilesString += ' ' + file.split('/')[-1]
            sandbox += '"{0}",'.format(JOB_LOCATION+str(self.id)+os.sep+'G-Fact/configurations/'+file.split('/')[-1])
        sandbox += '"{0}",'.format(JOB_LOCATION+str(self.id)+os.sep+mainConfigFileName)

        self.args += '--configFile {0} '.format(mainConfigFileName)
        self.args += '--configurationFiles'+configFilesString 
        self.sandbox += sandbox
        os.chdir(oldpwd)

        return

    #--------------------------------------------------------------------------
    # Submit and monitor functions
    #--------------------------------------------------------------------------

    def addURL(self):
        '''
        Adds a jobs url to the joblist.sh file, saving it.
        '''
        jl = openJoblist('append')
        jl.write(self.url)
        jl.write('\n')
        jl.close()
        return

    def submit(self):
        '''Submit the job'''
        
        self.write()

        path = JOB_LOCATION+str(self.id)+os.sep
        cmd = ['glite-ce-job-submit','--autm-delegation','-r','ce02.tier2.hep.manchester.ac.uk:8443/cream-pbs-mcore',path+self.jdl]
        output = subprocess.Popen(cmd,stdout=subprocess.PIPE ).communicate()[0]
        if output[0:4] == 'http':
            self.url = output.rstrip('\n')
            self.addURL()
            print('INFO: Job::submit: Job {0}, URL {1} has been submitted and saved'.format(self.id, self.url))
        else:
            print output
            print('INFO: Job::submit: Error Submitting Job {0}'.format(self.id))
        return
    
    def printJob(self):
        '''Prints the details of the job id given'''
        
        self.getStatus()
        self.getOutputStatus()
        if self.name == 'removed':
            return
        print self.id, '   |   ', self.name, '   |   ', self.status, '   |   GotOutput: ', self.outputStatus
        return

    def getStatus(self):

        if self.url == '':
            status = 'NOT SUBMITTED'
        else:
            cmd = ['glite-ce-job-status', self.url]
            output = subprocess.Popen(cmd,stdout=subprocess.PIPE ).communicate()[0]
            if 'DONE-OK' in output:
                status = 'DONE-OK'
            elif 'DONE-FAILED' in output:
                status = 'FAILED'
            elif 'IDLE' in output:
                status = 'IDLE'
            elif 'RUNNING' in output:
                status = 'RUNNING'
            elif 'REALLY-RUNNING' in output:
                status = 'REALLY-RUNNING'
            elif 'PENDING' in output:
                status = 'PENDING'
            elif 'ABORTED' in output:
                status = 'ABORTED'
            else:
                status = output
        self.status = status
        return

    def getStatusExtra(self):
        '''Gets the extra status and prints the raw output from glite'''

        if self.url == '':
            status = 'NOT SUBMITTED'
        else:
            cmd = ['glite-ce-job-status', '-L', '1', self.url]
            output = subprocess.Popen(cmd,stdout=subprocess.PIPE ).communicate()[0]
            print output

        return

    @checkForDir
    def getOutputStatus(self):
        '''Updates the output status of the job based on if it has a non empty output dir'''
        path = JOB_LOCATION+str(self.id)+os.sep
        if os.path.isdir(path+"outputs"+os.sep) == True:
            if os.listdir(path+"outputs"+os.sep) == []:
                self.outputStatus = False
            else:
                self.outputStatus = True
            return
        self.outputStatis = False
        return

    def getOutput(self, force=False):
        '''
        Makes a directory in the job directory and download the output there
            force(False) : get the output, even if the job isn't finished
        '''

        self.getStatus()
        if self.status != 'DONE-OK' and force == False:
            print("INFO: Job::getOutput: Can't get Job {0} output, it's not Finished Yet.".format(self.id))
            return

        #.root file
        success = True
        path = JOB_LOCATION+str(self.id)+os.sep
        if not os.path.isdir(path+'outputs'):
            os.mkdir(path+'outputs')
        vname = self.vostorename
        cmd = ['gfal-ls',MANVOLOC]
        lsout = subprocess.Popen(cmd,stdout=subprocess.PIPE ).communicate()[0]
        print('INFO: Job::getOutput: Getting output files from Job {0}'.format(self.id))
        for on, ft in zip(['output','stdout','stderr'],['.root','.log','.err']):
            if vname+ft not in lsout:
                print('INFO: Job::getOutput: File {0} not found in the output storage for Job {1}'.format(vname+ft,self.id))
                continue
            cmd = ['gfal-copy','{0}{1}'.format(MANVOLOC , vname+ft ),
                   'file:/{0}'.format(path+'outputs/'+on+ft)]
            output = subprocess.Popen(cmd,stdout=subprocess.PIPE ).communicate()[0]
            if 'WARN' in output or 'error' in output:
                if 'WARN' in output:
                    print('WARN: Job::getOutput: Problem downloading file {0} for Job {1}'.format(on+ft, self.id))
                if 'error' in output:
                    print('ERRO: Job::getOutput: Problem downloading file {0} for Job {1}'.format(on+ft, self.id))
                print output
                success = False

        #check success
        if success == True:
            self.getOutputStatus()
            if self.outputStatus == True:
                print('INFO: Job::getOutput: Files Downloaded Successfully for Job {0}'.format(self.id))
        return

    def getFormatOutput(self, outputdir, force=False):
        '''
        Gets the job output and copies it to outputdir in the format
        mode+mag+run+flavour.root, which it gets from the job name
        '''

        #make the directory
        if not os.path.isdir(outputdir):
            os.makedirs(outputdir)
        if not outputdir[-1] == os.sep:
            outputdir += os.sep

        #get the name
        for mode in MODES: 
            if mode in self.name: break
        for mag in MAGS: 
            if mag in self.name: break
        for run in RUNS: 
            if run in self.name: break
        #for fl in FLAVOURS:
        #    if fl == 'D0':
        #        if fl in self.name and 'D0bar' not in self.name:
        #            break
        #    else:
        #        if fl in self.name: break
        #oname = mode+'_'+mag+'_'+run+'_'+fl+'.root
        oname = mode+'_'+mag+'_'+run+'.root'

        #get the output
        self.getOutput(force)

        #copy the output
        path = JOB_LOCATION+str(self.id)+os.sep+'outputs'+os.sep
        os.system('cp {0} {1}'.format(path+'output.root',outputdir+oname))

        #delete the initial file
        os.system('rm {0}'.format(path+'output.root'))

    def getSystematicsFormatOutput(self, outputdir, force=False):
        '''
        Gets the job output and copies it to outputdir in the format
        mode+mag+run+flavour+syst.root, which it gets from the job name
        '''

        #make the directory
        if not os.path.isdir(outputdir):
            os.makedirs(outputdir)
        if not outputdir[-1] == os.sep:
            outputdir += os.sep

        #get the name
        for mode in MODES: 
            if mode in self.name: break
        for mag in MAGS: 
            if mag in self.name: break
        for run in RUNS: 
            if run in self.name: break
        #for fl in FLAVOURS:
        #    if fl == 'D0':
        #        if fl in self.name and 'D0bar' not in self.name:
        #            break
        #    else:
        #        if fl in self.name: break
        #oname = mode+'_'+mag+'_'+run+'_'+fl+'.root
        for syst in ['TPBias','TPScale','varyScndryTau','varyTimeRes']:
            if syst in self.name:
                syst_val = self.name.split(syst)[-1]
                break
        oname = mode+'_'+mag+'_'+run+'_'+syst+'_'+syst_val+'.root'

        #get the output
        self.getOutput(force)

        #copy the output
        path = JOB_LOCATION+str(self.id)+os.sep+'outputs'+os.sep
        os.system('cp {0} {1}'.format(path+'output.root',outputdir+oname))

        #delete the initial file
        os.system('rm {0}'.format(path+'output.root'))


    def removeJob(self):
        '''Removes the url from the joblist and removes the job directory'''

        #cancel job
        cmd = ['glite-ce-job-cancel', '-N',self.url]
        output = subprocess.Popen(cmd,stdout=subprocess.PIPE ).communicate()[0]
        print output

        self.purgeJob()

        #do nothing if removed already
        if self.name == 'removed':
            return

        #remove url from the joblist.sh file
        jl = openJoblist(opt = 'read')
        newfile = open(JOB_LOCATION+'joblist_temp.sh', 'w')
        lines = jl.readlines()
        i = 0
        for line in lines:
            if i == self.id:
                newfile.write('removed\n')
            else:
                newfile.write(line)
            i += 1

        #replace new with old
        path = JOB_LOCATION 
        os.system('mv {0}/joblist_temp.sh {0}/joblist.sh'.format(path))

        #clear the job directory
        path = JOB_LOCATION+str(self.id)+os.sep
        os.system('rm -rf {0}/G-Fact*'.format(path))
        os.system('rm -rf {0}/outputs/'.format(path))
        os.system('rm -rf {0}/mcore*'.format(path))
        os.system('rm -rf {0}/name'.format(path))
        self.setName('removed')

        if self.name == 'removed':
            print('INFO: Job::removeJob: Job {0} Removed Sucessfully'.format(self.id))

        self.joblist.remove(self)
        del self

        return    

    def purgeJob(self):
        '''Removes the files of the job on the WN and storage'''
        os.system('glite-ce-job-purge -N {0}'.format(self.url))
        for ft in ['.root','.log','.err']:
            vostorename = self.vostorename+ft
            cmd = ['gfal-rm', MANVOLOC+vostorename]
            output = subprocess.Popen(cmd,stdout=subprocess.PIPE ).communicate()[0]
        return


#--------------------------------------------------------------------------
# Monitor Fuunctions
#--------------------------------------------------------------------------

def jobs(jrange = None):
    '''
    prints a list of the jobs
    usage, no argument give all jobs
    int argument give job number int
    '0:1' will give first to second job (does this function work?)
    '''

    job = Job(loaded=True)
    joblist = job.joblist
    joblist.remove(job)
    del job
    returnjob = False
    print '-'*80
    if jrange == None:
        for j in joblist:
            if j.url != 'removed':
                j.printJob()
        returnjob = joblist
    elif type(jrange) == int:
        if jrange >= len(joblist) or jrange < 0:
            print 'Not A valid Job ID'
            return
        j = joblist[jrange]
        if j.url != 'removed':
            j.printJob()
        returnjob = j
    elif ':' in jrange:
        if '-' in jrange:
            print 'Not Valid Job IDs'
            return
        substart = int(jrange[0])
        subend = int(jrange[2])#doesnt work for double digits
        if substart > len(joblist) or substart < 0 or subend > len(joblist) or subend < 0 or subend < substart:
            print 'Not Valid Job IDs'
            return
        sublist = joblist[substart:subend]
        for j in sublist:
            if j.url != 'removed':
                j.printJob()
        returnjob = sublist
    print '-'*80
       
    if returnjob == False:
        print 'There are no Jobs at this location'

    return returnjob

def look(path = MANVOLOC):
    '''look in the MANVOLOC dir'''
    
    cmd = ['gfal-ls', '{0}'.format(path)]
    output = subprocess.Popen(cmd,stdout=subprocess.PIPE ).communicate()[0]
    print output
    
    return

#--------------------------------------------------------------------------
# Multicore functions
#--------------------------------------------------------------------------

def newJob(name = '', exe = '', args = '', output = '', configFiles = [], dataFiles = [] , timePlotsFile = ''):
    '''
    make a new job
        args : the command line arguments after the run exe command
        exe  : the exe path and name, 
    '''
    job = Job(jid = None, url = None, name = name, exe = exe, args = args , output=output, timePlotsFile = timePlotsFile)
    job.getConfigArgs(configFiles)
    job.setupData(dataFiles)
    return job

def openJoblist(opt = 'read'):
    '''
    Opens and returns the joblist.sh
    Creates the directory and file if it doesnt exist
    write option is not used
    '''
    gfactdir = main_gfactdir
    jobdir = '/'.join(JOB_LOCATION.rsplit('/')[:-2])+'/'
    finaloc = JOB_LOCATION.rsplit('/')[-2]
    if os.path.isdir(jobdir) == True:
        if os.path.isdir(JOB_LOCATION) != True:
            os.system('mkdir {0}'.format(JOB_LOCATION))
    else:
        print('ERRO: multicore::openJoblist: The JOB_LOCATION, {0}, doesnt exist.'.format(JOB_LOCATION))
        sys.exit()
    try:
        if opt == 'read':
            jl = open(JOB_LOCATION+'joblist.sh', 'r') 
        elif opt == 'append':
            jl = open(JOB_LOCATION+'joblist.sh', 'a') 
        elif opt == 'write':
            jl = open(JOB_LOCATION+'joblist.sh', 'rw') 
    except IOError:
        if opt == 'read':
            jl = open(JOB_LOCATION+'joblist.sh', 'w')
            jl.close()
            jl = open(JOB_LOCATION+'joblist.sh', 'r')     
        elif opt == 'append':
            jl = open(JOB_LOCATION+'joblist.sh', 'w') 
        else:
            print('ERRO: multicore::openJoblist: Problem opening joblist.sh file.')
    return jl
    
def initJobs():
    '''
    Inits the jobs which already exist, gets jobs from joblist.sh
    joblist.sh is a list of the urls
    '''

    jl = openJoblist()
    lines = jl.readlines()
    njobs = len(lines)

    #make jobs with the urls
    for i in range(0, njobs):
        Job(i, lines[i].rstrip('\n'), loaded=True)
    jl.close()

    #get the job names and sh and jdl paths
    for job in Job.joblist:
        job.getName()
        job.getVOStore()
        job.getJDL()
        job.getSH()
        job.getStatus()
        job.getOutputStatus()
        job.readOutputName()

    return

#--------------------------------------------------------------------------
# Other Functions
#--------------------------------------------------------------------------

def help():
    '''Shows list of commands with a description'''

    breaker = '-'*80
    print breaker
    print 'jobs(): displays jobs'
    print "  * Ex. jobs(), jobs(2), jobs('0:2')"
    print breaker
    print "Job(id = len(joblist)+1, url = '', name = '', exe = '', args = '', jdl = '', sh = '', sandbox = '', output = '', status = '', outputStatus = '')"
    print "  * jdl and sh are file names not locations, exe is a location and/or a name"
    print "  * args are the command line arguments after the run exe command"
    print "  * sandbox is a list of str sanbox file names"
    print "  * output is a str list of output file names"
    print breaker
    print "Job() functions are getOutput(), getStatus(), getStatusExtra(), printJob(), getOutputStatus(), submit(), removeJob(), purgeJob()"
    print breaker
    print "newJob(name = '', exe = '', args = '', configFiles = [], dataFiles = [])"
    print "  * args are the command line arguments after the run exe command"
    print "  * exe is the exe path plus name"
    print breaker
    print "look(path = '')"
    print "  * look into home directory on the manchester CE(Computing Element)"
    print "  * path allows the user to look into subdirectories"


#--------------------------------------------------------------------------
# Main function
#--------------------------------------------------------------------------

def main():
    print 'Have You Setup Your Northgrid Proxy???'
    initJobs()
    return

main()
    





        
