#submission script for a multicore jobs, standard and systematics
#
#Kevin Maguire
#11/04/2015
#

#--------------------------------------------------------

from multicore import *
import os
#from TreeCopier import *
sys.path.append('/afs/cern.ch/user/m/masmith/cmtuser/Erasmus_v10r0/PhysFit/G-Fact/src/G-Fact/tools/Multicore/')
#from options import *

#--------------------------------------------------------

gfactdir = os.environ['GFACTDIR']
exepath = gfactdir+os.sep+'G-Fact/main/G-Fact.exe'
#exepath = gfactdir+os.sep+'G-Fact/main/G-Fact-5-34-24.exe'
namestr_extra = ''
#MODES = ['kk','pipi']#['kk','pipi']#['kpi', 'kk', 'pipi']
#MODES = ['kk','pipi']
MODES = ['kk', 'pipi']
#MODES = ['pipi']
FULL_KPI_MAGDOWN = True
MAGS = ['MagUp', 'MagDown']
#MAGS = ['MagDown']
RUNS = ['run1', 'run2', 'run3']#['split{0}'.format(i) for i in range(1, 18)]
#RUNS = ['run3']#['split{0}'.format(i) for i in range(1, 18)]
#FLAVOURS = ['D0','D0bar']
FLAVOURS = ['D0']
#SYSTS = {'splitEta_0' : ' --minEta 0 --maxEta 3 ' ,'splitEta_1' : ' --minEta 3 --maxEta 3.5 ' , 'splitEta_2' : ' --minEta 3.5 --maxEta 6 ' , 'splitOpening_0' : ' --minOpenAngle 0 --maxOpenAngle 0.996 ' , 'splitOpening_1' : ' --minOpenAngle 0.996 --maxOpenAngle 0.998 ' , 'splitOpening_2' : ' --minOpenAngle 0.998 --maxOpenAngle 0.999 ' , 'splitOpening_3' : ' --minOpenAngle 0.999 --maxOpenAngle 2. ' , 'splitDIRA_0' : ' --minMother_DIRA 0.99985 --maxMother_DIRA 0.99995 ' , 'splitDIRA_1' : ' --minMother_DIRA 0.99995 --maxMother_DIRA 0.99998 ', 'splitDIRA_2' : ' --minMother_DIRA 0.99998 --maxMother_DIRA 2. '}
#SYSTS = {'splitDIRA_2' : ' --minMother_DIRA 0.99998 --maxMother_DIRA 2. '}
#SYSTS = {'splitPT_0' : '--minMother_PT 0 --maxMother_PT 3300 ', 'splitPT_1' : '--minMother_PT 3300 --maxMother_PT 4300 ', 'splitPT_2' : '--minMother_PT 4300 --maxMother_PT 6000 ', 'splitPT_3' : '--minMother_PT 6000 --maxMother_PT 999999 '}
#SYSTS = {'splitVtx_0': ' --minMother_vtxChi2 0 --maxMother_vtxChi2 1 ', 'splitVtx_1': ' --minMother_vtxChi2 1 --maxMother 2', 'splitVtx_2': ' --minMother_vtxChi2 2 --maxMother_vtxChi2 4 ', 'splitVtx_3': ' --minMother_vtxChi2 4 --maxMother_vtxChi2 10 '
#}
#SYSTS = {'std' : ' '}
SYSTS = {
    #'TPBias-0.005' : ' --d-TPBias -0.005 '#,
    #'TPBias-0.003' : ' --d-TPBias -0.003 ',
    #'TPBias-0.001' : ' --d-TPBias -0.001 ',
    #'TPBias0.001' : ' --d-TPBias 0.001 ',
    #'TPBias0.003' : ' --d-TPBias 0.003 ',
    #'TPBias0.005' : ' --d-TPBias 0.005',
    #'TPScale0.9999' : ' --d-TPScale 0.9999 ',
    #'TPScale0.99995' : ' --d-TPScale 0.99995 ',
    #'TPScale1.00005' : ' --d-TPScale 1.00005 ',
    #'TPScale1.0001' : ' --d-TPScale 1.0001 '
    #'varyScndryTau0.17' : ' --d-scndry_tau1 0.17 ',
    #'varyScndryTau0.24' : ' --d-scndry_tau1 0.24 ',
    #'varyScndryTau0.27' : ' --d-scndry_tau1 0.27 ',
    #'varyScndryTau0.35' : ' --d-scndry_tau1 0.35 ',
    #'varyScndryTau0.4' : ' --d-scndry_tau1 0.4 '
    'varyTimeRes0.03' : ' --d-avgProperTimeErr 0.03 ',
    'varyTimeRes0.04' : ' --d-avgProperTimeErr 0.04 ',
    'varyTimeRes0.06' : ' --d-avgProperTimeErr 0.06 ',
    'varyTimeRes0.07' : ' --d-avgProperTimeErr 0.07 ',
    'varyTimeResMulti' : ' --avgProperTimeErr 0.032 --avgProperTimeErr_1 0.065 --avgProperTimeErr_2 0.38 --timeSigmaFrac_0 0.714 --timeSigmaFrac_1 0.282 --nTimeSigmas 3 '
    }
#extra_config_dir = 'S20/splits'--applyDecayTimeCorrection 1
extra_config_dir = 'S20'
#MANVOLOC = 'srm://bohr3226.tier2.hep.manchester.ac.uk/dpm/tier2.hep.manchester.ac.uk/home/vo.northgrid.ac.uk/kmaguire/jobs/'
EXTRA_ARGS = ' --saveTimePlots 1 --save2D 1 --outputFile output.root --minT 0.25e-3  --useExpPlusExpCrossExp 0 --massDepLogIP 1 --addRecoEff 0 --D_Dbar_both 0   --Fitter sFit   --doSFit 1 --scndry_IP_Mu_V5 1 0 0.01 --scndry_IP_Mu_V6 1 3 0.01 --scndry_IP_Mu_V7 1 1 0.01  --signal_IP_Mu_DepType Prompt --minK_PIDK 10 --D_Dbar_both 2'
#EXTRA_ARGS += ' --maxEntries 200000 '

#--------------------------------------------------------
# Options
#--------------------------------------------------------
MC = False
SYSTEMATICS = False

#------------------------------------------------------------------------------
# Main
#------------------------------------------------------------------------------

def main():
    
    #loop over each subsample make configFiles list
    config_args_GLOBAL = ['D2hh_2012/blinding_seeds_charm2012.txt', 'D2hh_2012/CombinedFitter_TimeFitCommon.txt']
    for mode in MODES:
#        config_args_mode = config_args_GLOBAL + ['D2hh_2012/'+mode+'/CombinedFitter_'+mode+'_CombinedData.txt']

        config_args_mode = config_args_GLOBAL 
#        config_args_mode += ['D2hh_2012/S20/'+mode+'']
        for mag in MAGS:
            for run in RUNS:
              for syst in SYSTS:
                systarg = '  ' + SYSTS[syst]
                if run == '' and mag == '':
                    mag_run = ''
                elif mag == '':
                    mag_run = '_'+run
                else:
                    mag_run = '_'+mag+'_'+run 
                for flavour in FLAVOURS:
                    if flavour == '':
                        mode_flavour = mode+'/'
                    else:
                        mode_flavour = mode+'/'+flavour+'/'
                    runArg = ''
                    if run == 'run1':
                      runArg = 'RunBlock1'
                    if run == 'run2':
                      runArg = 'RunBlock2'
                    if run == 'run3':
                      runArg = 'RunBlock3'
                    config_args_subsample = config_args_mode + ['D2hh_2012/'+extra_config_dir+'/'+mode_flavour+'data'+mag_run+'.txt']
                    configFiles = config_args_subsample + ['D2hh_2012/'+mode+'/CombinedFitter_TimeFit_'+mode+'_'+flavour+'_'+mag+'_'+runArg+'.txt']
                    configFiles += ['D2hh_2012/'+mode+'/CombinedFitter_'+mode+'_'+flavour+'_'+mag+'_'+runArg+'.txt']
#                    configFiles = config_args_mode               
                    ##remove some subsamples?
                    #if 'kk' == mode and 'MagDown' == mag and 'run1' == run and 'D0' == flavour:
                    #    print 'WARN: Removing subsample ', mode, mag, run, flavour
                    #    continue

                    #make data file list
                    dataFiles = []
                    if 'split' in run:
                        dataFiles.append(dataFilesConfig[mode][split_mag_lookup[run]]['signal'])
                        dataFiles.append(dataFilesConfig[mode][split_mag_lookup[run]]['widemass'])
                    else:
                        dataFiles.append(dataFilesConfig[mode][mag]['signal'])
                        dataFiles.append(dataFilesConfig[mode][mag]['widemass'])

                    if SYSTEMATICS == True:
                        pass #use the normal code
                        '''
                        for t, (test) in enumerate(systematicsTests):                        

                            #only run some?
                            #if test not in ['indepSlPiMass', 'scaleSlpiTau','splitSlowPiPIDe','varyMaxT']:
                            #if test not in ['varyTimeResMulit','varyBandWidthScale','SingleScndryTimePDF','varyScndryTau1','addRecoEff','varyTimeRes']: 
                            if test not in ['varyTPScale']: #['varyTPBias','varyTPScale']: 
                                print 'WARN: Removing test ', test
                                continue
 
                            #check mode specific                                                                                    
                            if 'finalState' in systematicsTests[test]:
                                if mode != systematicsTests[test]['finalState']: continue
                            #subtests
                            for k, (subtest) in enumerate(systematicsTests[test]['subtestID']):                                      
                                cutargs = systematicsTests[test]['cut'][k] + '  --ignoreFailed'
                                name = mode + mag_run +'_'+ flavour +'_'+ test +'_'+ subtest + namestr_extra

                                #remove subtests
                                if subtest != '1':continue

                                rootOutput = 'outputs/'+mode + mag_run +'_'+ flavour + '.root'
                                job = newJob(name, exepath, cutargs, configFiles, dataFiles,'timePlots.root') 
                                print name
                                print cutargs
                                job.submit()
                                '''
                    else:
                        args = EXTRA_ARGS + ' ' + systarg
                        name = mode+mag_run+'_'+flavour+namestr_extra+'_'+syst
                        rootOutput = 'outputs/'+mode + mag_run +'_'+ flavour+'.root'
                        #name, exepath, args, configFilelist, dataFilelist
                        job = newJob(name, exepath, args, rootOutput, configFiles, dataFiles) 
                        job.submit()
                        #sys.exit()

#-----------------------------------------------------------------------
# Config dicts
#-----------------------------------------------------------------------

#the directory where the job output will be stored
manvostore = namestr_extra.replace('_','')
"""
dataFilesConfig = {}
if MC == False:
    dataFilesConfig = {'kk':{'MagDown': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kk_SIG_swimming_merged_MagDown.root',
                                         'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kk_WM_swimming_merged_MagDown.root'},
                             'MagUp': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kk_SIG_swimming_merged_MagUp.root',
                                       'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kk_WM_swimming_merged_MagUp.root'}},
                       'kpi': {'MagDown': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_SIG_swimming_merged_MagDown.root',
                                           'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_WM_swimming_merged_MagDown.root'},
                               'MagUp': 
                               {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_SIG_swimming_merged_MagUp.root',
#                                'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_WM_swimming_merged_MagUp.root'}},
                                'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_WM_swimming_merged_MagUp_Reload.root'}},
                       'pipi': {'MagDown': 
                                {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2pipi_SIG_swimming_merged_MagDown.root',
                                 'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2pipi_WM_swimming_merged_MagDown.root'},
                                'MagUp': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2pipi_SIG_swimming_merged_MagUp.root',
                                          'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2pipi_WM_swimming_merged_MagUp.root'}}
                       }
if FULL_KPI_MAGDOWN == True:
    dataFilesConfig['kpi']['MagDown']['signal'] = 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_SIG_swimming_merged_MagDown_full.root'
    
elif MC == True:
    dataFilesConfig = {'kpi': {'MagDown': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_SIG_swimming_merged_MagDown_prompt.root',
                                           'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_WM_swimming_merged_MagDown_prompt.root'},
                               'MagUp': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_SIG_swimming_merged_MagUp_prompt.root',
                                         'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_WM_swimming_merged_MagUp_prompt.root'}},
                       }
"""
dataFilesConfig = {
        'kk':
            {'MagDown': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kk_SIG_swimming_merged_MagDown_full.root',
                         'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kk_WM_swimming_merged_MagDown_full.root'},
             'MagUp': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kk_SIG_swimming_merged_MagUp_full.root',
                       'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kk_WM_swimming_merged_MagUp_full.root'}
             },
        'kpi':
        {'MagDown': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_SIG_swimming_merged_MagDown_full.root',
                     'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_WM_swimming_merged_MagDown.root'},
         'MagUp':
         {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_SIG_swimming_merged_MagUp_full.root',
          'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2kpi_WM_swimming_merged_MagUp_full.root'}
         },
        'pipi':
        {'MagDown':
         {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2pipi_SIG_swimming_merged_MagDown_full.root',
          'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2pipi_WM_swimming_merged_MagDown_full.root'},
         'MagUp': {'signal': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2pipi_SIG_swimming_merged_MagUp_full.root',
                   'widemass': 'root://bohr3226.tier2.hep.manchester.ac.uk:1094//dpm/tier2.hep.manchester.ac.uk/home/lhcb/lhcb/user/k/kmaguire/D2pipi_WM_swimming_merged_MagUp_full.root'}
}
}
    
#look up dict for the mag polarity of the different run splits
split_mag_lookup = {'split1' : 'MagDown', 'split2' : 'MagDown', 'split3' : 'MagUp', 'split4' : 'MagDown', 'split5' : 'MagUp', 'split6' : 'MagDown',
                    'split7' : 'MagUp', 'split8' : 'MagUp', 'split9' : 'MagUp', 'split10' : 'MagDown', 'split11' : 'MagUp', 'split12' : 'MagDown',
                    'split13' : 'MagUp', 'split14' : 'MagDown', 'split15' : 'MagUp', 'split16' : 'MagDown', 'split17' : 'MagUp' }


main()
